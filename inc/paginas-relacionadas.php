<div class="col-12 mt-4">
<h2 class=" mb-3">Publicações Relacionadas</h2>

    <?php $limit = 3;

    include('inc/vetKey.php');
    include('inc/classes/strFuncoes.class.php'); 
    $str = new strFuncoes();
    shuffle($vetKey);
    $strPalavra = implode(" ", $str->RetiraPreposicao($h1));
       


    $vetKey_PR = $str->buscaVetorAprox($strPalavra, $vetKey, $limit, $h1);
        
    
    for ($i = 0; $i < sizeof($vetKey_PR); $i++) { ?>

    <a rel="nofollow" href="<?=$url?><?=$vetKey[$vetKey_PR[$i]]['url']; ?>" title="<?=$vetKey[$vetKey_PR[$i]]['key']; ?>" >
	<div class="card mb-3 related-posting w-100">
		<div class="row align-items-center">
			<div class="col-md-2">
				<img src="<?=$url?>assets/img/img-mpi/250x150/<?=$vetKey[$vetKey_PR[$i]]['url']?>-1.jpg" alt="<?= $vetKey[$vetKey_PR[$i]]['key']; ?>" title="<?= $vetKey[$vetKey_PR[$i]]['key']; ?>">
			</div>
			<div class="col-md-10">
				<div class="card-body">
					<h2 class="card-title text-center text-md-left" style="text-transform:uppercase;margin:0px"><?= $vetKey[$vetKey_PR[$i]]['key']; ?></h2>
				</div>
			</div>
		</div>
	</div></a>

<?php } ?>
</div>