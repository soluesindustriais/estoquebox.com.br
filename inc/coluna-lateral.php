<aside class="col-md-3 col-12 m-0">

    <button id="btnOrcamento" class="btn-orcar botao-cotar"><i class="fa fa-envelope"></i>
        Solicite um Orçamento
    </button>

    <script src="//www.solucoesindustriais.com.br/assets/js/plugin/telefones.min.js"></script>
    <script>
        pluginTelefonesJs.init({
            industria: 'solucoes-industriais',
            btnTelefone: '#btnTelefone',
            titulo: 'h1'
        });
    </script>
    
    <button id="btnTelefone" class="btn-tel"><i class="fa fa-phone" aria-hidden="true"></i>
        Entrar em Contato
    </button>
    <div class="d-none d-md-block w-100">
        <a href="<?=$url?>informacoes" class=" list-group-item aside-title" title="Informações">
            <h2 class="text-uppercase">Informações</h2>
        </a>
        <?php
	function submenu($buffer)
	{
		return (str_replace("dropdown-item", "list-group-item", $buffer));
	}
	ob_start("submenu");
	echo "<ul class=\"menu-late\">";
	echo "<li>";

	include 'inc/sub-menu-informacoes.php';
	echo "</li>";
	echo "</ul>";
	ob_end_flush();
	?>
    </div>


</aside>
