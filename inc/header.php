<header class="sticky-top">
    <nav id="menu" class="navbar navbar-expand-md navbar-light">
        <div class="container" style="padding:0px 15px;width:100%;">
            <a class="navbar-brand" href="<?=$url?>" title="<?=$nomeSite." - ".$slogan?>"><img src="<?=$url?>assets/img/logo.png" alt="<?=$nomeSite." - ".$slogan?>" title="<?=$nomeSite." - ".$slogan?>"></a>
            <button class="navbar-toggler ml-auto mr-3" type="button" data-toggle="collapse" data-target="#navbar1" aria-controls="navbar1" aria-expanded="false" aria-label="Toggle navigation" style="float:right">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar1">
                <ul class="navbar-nav ml-auto">
                    <?php include 'inc/menu.php' ?>
                </ul>
            </div>
        </div>
    </nav>
</header>
<?php if ($title != "Home") { ?>
    <section class="mb-5 banner-brad">
        <div class="w-100 h-100" style="background:rgba(0,0,0,0);padding:50px 0px">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1 class="text-white text-center" style="text-shadow: 1px 1px 2px #000000;"><?= $h1 ?></h1>
                        <div class="breadcrumb" id="breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                        <a href="<?= $url ?>" itemprop="item" title="Home">
                                            <span itemprop="name">Home</span>
                                        </a>
                                        <meta itemprop="position" content="1" />
                                    </li>
                                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                        <a href="<?= $url ?>/informacoes" itemprop="item" title="informacoes">
                                            <span itemprop="name">informações</span>
                                        </a>
                                        <meta itemprop="position" content="2" />
                                    </li>
                                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                        <span itemprop="name"> <?= $h1 ?> </span>
                                        <meta itemprop="position" content="3" />
                                </ol>
                            </nav>
                        </div>
                        <?php if (isset($pagInterna) && ($pagInterna != "")) {
                            $previousUrl[0] = array("title" => $pagInterna);
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>