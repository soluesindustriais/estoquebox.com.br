The MIT License (MIT)

Copyright (c) 2016 Aigars Silkalns & Colorlib

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

#################################################################################
################################ S I G & MPI ####################################
#################################################################################
Framework Doutor Web 2016® v.0.1b

Módulos de recursos disponíveis

Acesso administrativo
Administração de usuários com até 3 níveis de acesso
Histórico de operações do sistema
Recursos
Importador de Dados
Categorias
Quem somos
Orçamentos (Carrinho de orçamentos)
Novidades
Blog
Produtos
Serviços
Banner
Fornecedores/Distribuidores
Clientes/Quem atendemos
Cases
Downloads
Vagas
Candidatos
Newsletter
Páginas

1.a. Administração de usuários com até 3 níveis de acesso
Cadastro de usuários com senha auto-gerada e enviada por e-mail com informações de acesso. Possibilidade de administrar os níveis de usuários
Nível 1 - Colaborador (Terá acesso somente a visualizar as informações de recursos do sistema)
Nível 2 - Supervisor (Terá acesso a visualizar, cadastrar e editar informações do sistema)
Nível 3 - Gestor (Terá acesso a área administrativa do sistema, além de poder visualizar, cadastrar, editar e excluir informações do sistema.

1.b. Histórico de operações do sistema
Somente usuários com nível 3 terão acesso à estas informações, é possível filtrar as buscas através do campos de pesquisa instantânea.

2.a. Importador de Dados
Recurso que permite importar informações através de um arquivo .csv formatado no modelo do sistema, para o uso de recurso é necessário consultar a equipe técnica para liberar o nível de acesso necessário.

2.b. Categorias
Administração de seções, categorias e subcategorias, possibilidade e adicionar capa as seções, categorias e subcategorias, somente é possível administrar e implementar até 3 níveis de categorias como no exemplo abaixo:

Seção Produtos > Categoria Máquinas > Subcategoria Maquinas de coxinha

Os módulos funcionam de acordo com a seção criada, para que o módulo de Download apareça no menu de itens do site, é necessário existir uma seção com este mesmo nome, assim como Produtos, Serviços entre outros. Nem todos os Módulos necessitam de seções, como o de Banners, Newsletter e páginas. Dependendo do nome da seção o nome físico do arquivo deve ser o mesmo da URL amigável, como por exemplo: seção Produtos o arquivo deve se chamar produtos.php.

2.c. Quem somos
Recurso dedicado ao conteúdo de empresa, institucional, história, entre outros, este recurso assim como outros também trabalha com categorias e subcategorias. O nome da seção geralmente é Empresa ou Quem somos, assim no “Quem somos” a URL ficaria “quem-somos.php”.

2.e. Orçamentos
Recurso ligado a dois tipos de entrada.
Carrinho de orçamento, os itens solicitados no site via carrinho de orçamento são gravados neste recurso e ficam disponíveis para exportação em Excel, CSV, PDF, Imprimir ou Copiar linhas.
Seção de orçamentos, pode-se criar uma seção onde os cliente preenchem o formulário de orçamento e enviam a solicitação para o sistema com cópia para e-mail, a mesma funcionalidade está disponível no carrinho de orçamentos também.

2.e. Novidades
Recurso voltado para expor novidades , seja da empresa, feiras, propagandas sobre novos produtos. Este recurso trabalha com uma include na home do site, para que o cliente visualize o item como um item novo adicionado.

2.f. Blog
Recurso voltado ao cadastro de artigos, documentários, assunto mais complexos, dicas, tutoriais, este recurso não deve ser confundido com o recurso de Novidades, ambos tem focos diferentes.

2.g. Produtos
Recurso mais completo disponível no sistema, o mesmo trabalha com o cadastro de produtos com a possibilidade de:
Definir uma capa principal
Definir um código 
Preço antigo
Preço atual
Palavras chave para metas keywords
Nome do produto, não permite que cadastre um produto com o mesmo nome e gera a URL amigável automaticamente
Descrição, seguindo as regras básica de MPI, mínimo 140 e Máximo de 160 caracteres.
Conteúdo da página, com a possibilidade de enviar imagens e incorporar dentro do corpo do conteúdo, incorporar vídeos do youtube através da URL, editar diretamente o código fonte (HTML) para quem tem conhecimentos mais avançados, inserir tabelas e trabalhar o alinhamento como se estivesse utilizando um um word pad. Os formatos disponíveis começam a partir do H2, para seguir a regra de apenas um H1 por página.
Envio de até 6 imagens para a galeria do produto.
Envio instantâneo de arquivos .pdf, .xlsx, .csv para incorporar à aba de downloads do produto
Recurso integrado com Downloads possibilitando pesquisa na busca instantânea um arquivo cadastrado no sistema e relacionar ao produto.
Sistema de múltiplas categorias, o mesmo produto pode ser adicionado a mais de uma categoria ou subcategoria.

2.h. Serviços
Recurso voltado a expor os tipos de serviços que a empresa faz, o recurso permite assim como o de produtos, adicionar arquivo anexados para maiores informações dos clientes, caso existam tabelas técnicas, protocolos entre outros.
As mesmas funcionalidades do recurso de produtos estão disponíveis para Serviços, com exceção da Multiplas categorias.
2.i. Banner
Recurso que cadastra imagens na como Slide Show e geralmente são exibidos na Home page do site. 
Este recurso permite adicionar qualquer link cadastrado no sistema através de um campo de select, a URL é opcional, caso não seja cadastrado o banner sempre levará para Home page do site. Também pode-se definir um enunciado que aparecerá em formato de texto por cima da imagem do banner dependendo do modelo de layout adotado e da configuração do CSS.

2.j. Fornecedores/Distribuidores
Recurso voltado ao cadastro de logos dos distribuidores ou fornecedores, o recurso permite que seja cadastrado um site também que é opcional.

2.k. Clientes/Quem atendemos
Recurso voltado ao cadastro de logos dos cliente que são atendidos pela empresa, o recurso possui as mesmas funcionalidade de Fornecedores mas voltados a clientes.

2.l. Cases
Recurso voltado ao cadastro dos itens de portfólio do cliente, como cases de sucesso, trabalhos executados, entre outros, o recurso também opera com categorias e subcategorias para melhor organização do itens cadastrados. Este recurso permite o cadastro de até 20 imagens por case.

2.m. Download
Recurso que opera incorporado em Produtos e Serviços, com a possibilidade de cadastrar arquivos do tipo .pdf, .doc, .xlsx e .csv. O recurso também opera com categorias e subcategorias para melhor organização de seus itens. Assim como outros recursos ele deve possuir uma seção caso haja a necessidade de exibir os downloads no site.
Este recurso é voltado ao cadastro de catálogos, certificados, tabelas técnicas, manuais de uso, entre outros.

2.n. Vagas
Este recurso trabalha com a página trabalhe-conosco.php ou Seção Trabalhe conosco. Possibilidade de cadastrar uma área como RH e em seguida uma vaga para auxiliar de RH por exemplo. É necessário criar uma área para cadastrar uma determinada vaga para melhor organização.
O itens aqui cadastrados ficam expostos no site de forma que o usuário pode se candidatar à uma área específica através do formulário de trabalhe conosco. O recurso funciona diretamente incorporado com o recurso de Candidatos. 

2.o. Candidatos
Recurso que trabalha incorporado para receber os candidatos do recurso Vagas, nele é possível visualizar os curriculo enviado pelo usuário e excluir o mesmo caso necessário, também é possível visualizar algumas informações básicas antes mesmo de abrir o currículo.

2.p. Newsletter
Recurso para captar lista de emails através do preenchimento dos campos nome e email no site. Este recurso trabalha somente com a exportação das lista de emails cadastradas.
Ele conta com:
Envio de e-mail com chave única com link de verificação do recebimento
Possibilidade excluir e-mails da lista
Possibilidade de exportar lista para: Excel, CSV, PDF, copiar linhas, imprimir.

2.q. Páginas
Recurso que possibilita a criação de páginas sem a necessidade de definir uma categoria ou subcategoria como se fossem páginas estáticas no site. O recurso possibilita o cadastro de:
Nome da página, não permite que cadastre uma página com o mesmo nome e caso exista uma seção no sistema com o mesmo nome também não permite a URL amigável é gerada automaticamente
Descrição, seguindo as regras básica de MPI, mínimo 140 e Máximo de 160 caracteres.
Conteúdo da página, com a possibilidade de enviar imagens e incorporar dentro do corpo do conteúdo, incorporar vídeos do youtube através da URL, editar diretamente o código fonte (HTML) para quem tem conhecimentos mais avançados, inserir tabelas e trabalhar o alinhamento como se estivesse utilizando um um word pad. Os formatos disponíveis começam a partir do H2, para seguir a regra de apenas um H1 por página, entre outros.

Scopo do sistema

O sistema é 100% responsivo, sendo assim funcionando em dispositivos mobile e atendendo também as necessidade do Cross-Browser.
Trabalha com PDO em alta performance abrindo a conexão, efetuando a consulta e fechando a conexão. 

Sua arquitetura segue um modelo próprio de framework com um front-controller baseado em URLs tratadas para os níveis de acesso e rotas pré-definidas.
Foi feito o uso de um template bootstrap gratuito que possui sua licença de uso na raiz do sistema.

Desenvolvido em PHP Orientado a Objetos o sistema possui documentação de funções, classes, métodos inseridas em seu corpo auxiliando o uso e o reuso das mesmas.
Atualmente funcionando nas versões de PHP 5.3 a 5.6. O sistema possui uma área de administração master onde é possível administrar a empresa para o qual ele foi criado, assim qualquer analista pode logar com o usuário e senha padrão e acessar todos os recursos do sistema com total liberdade de um usuário Nível 5.


