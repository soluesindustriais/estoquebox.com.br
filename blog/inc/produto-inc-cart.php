<form class="cart j_Cart" method="post" enctype="multipart/form-data">
  <input type="hidden" name="base" value="<?=BASE?>" class="j_base">
  <input type="hidden" name="prod_id" value="<?= $prod_id; ?>">
  <input type="hidden" name="prod_codigo" value="<?= $prod_codigo; ?>">
  <input type="hidden" name="action" value="addCart">
  <?php if (!empty($prod_atributos) && $prod_atributos != " "): ?>
    <fieldset>
      <legend>Aplicação</legend>
      <?php
      $atributos = explode(",", $prod_atributos);
      foreach ($atributos as $keys): ?>
        <label class="radio-option">
          <input type="radio" name="prod_item" value="<?= $keys; ?>" required/><?= $keys; ?>
        </label>
        <?php
      endforeach; ?>
    </fieldset> 
  <? endif; ?>
  <div class="row justify-content-center">
    <div class="p-2 col-6 col-sm-6 col-xs-12">
      <input class="qtdCart" type="number" name="prod_qtd" min="1" step="1" value="1">
    </div>
    <div class="p-2 col-6 col-sm-6 col-xs-12">
      <input class="addOrc j_Cart" type="submit" name="Adicionar" value="Adicionar">
    </div>
  </div>
</form>