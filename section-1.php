<section class="px-1 my-5">
	
    <div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="mb-5 mt-3 col-12 text-center text-destaq text-black" style="color:#000">Conheça nossos produtos</h2>
				<span class="line-yellow my-2"></span>
			</div>
		</div>
		
	</div>

	<div class="card-deck m-0 mt-3">
		<div class="container">
			<div class="justify-content-around owl-carousel">
            <?php  
                $palavraEstudo_s7 = array(
                'selfstorage',
                'espaço depósito',
                'guarda móveis sp',
                'self storage sp',
                'armazenagem logística',
                'armazenamento de produtos químicos',
                'box armazenamento',
                'box para alugar'
            );
                
            include 'inc/vetKey.php';            
            asort($vetKey);
            foreach ($vetKey as $key => $value) {
                
            if(in_array(strtolower($value['key']), $palavraEstudo_s7)){  ?>        
                
                
 
				<div class="col-12 p-0">
						<a href="<?=$url.$value["url"];?>" title="<?=$value['key']?>">
                                <div class="cards border">
                                    <img class="card-img-top new-card" src="<?=$url?>assets/img/img-mpi/350x350/<?=$value['url']?>-1.jpg" alt="<?=$value['key']?>" title="<?=$value['key']?>">                                    
                                    <div class="card-footer p-0 py-2" style='height:55px'>
                                    <h2 class="text-center m-0" style="font-weight:500"><?=$value['key']?></h2>                                    
                                </div>
                            </div>
                            </a>
                        </div>
				
             <?php  } } ?>
			</div>
		</div>
	</div>
</section>