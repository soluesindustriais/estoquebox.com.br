<?php
include('inc/vetKey.php');
$h1 = "self box";
$title = $h1;
$desc = "Por que fazer a escolha de um self box? Quando as pessoas começam a perceber que sua casa ou apartamento já não comportam da melhor forma seus móveis";
$key = "self,box";
$legendaImagem = "Foto ilustrativa de self box";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php include('inc/head.php'); ?>
    <link rel="stylesheet" href="<?=$url?>assets/css/mpi-style.css">
</head>

<body>
    <?php include 'inc/header.php' ?>   
        <div class="container">
            <div class="row">
                <article class="col-md-9 col-12">
                    <?php $quantia = 3;
                    include('inc/gallery.php'); ?>

                    <h2>Por que fazer a escolha de um self box?</h2><p>Quando as pessoas começam a perceber que sua casa ou apartamento, já não comportam da melhor forma seus móveis ou objetos maiores que foram adquiridos ao longo dos anos, é preciso tomar alguma providência para solucionar esse problema. Uma boa alternativa é o self box que permite que o cliente coloque suas coisas em um lugar mais seguro sem ter que tirar uma parte de seu espaço dentro de casa para isso. Essa modalidade não é tão popular no Brasil, mas é possível ver sua eficiência em outros países que possuem a cultura do self box.</p><h2>Motivos para optar pelo self box</h2><p>Algumas pessoas demoram para perceber que suas coisas estão ocupando mais espaço dentro de casa do que deviam e que é o momento adequado para contratar um self box. No entanto, também é preciso que a pessoa tenha a certeza de que é preciso alugar um serviço desse. Veja os principais motivos desse tipo de aluguel:</p><ul><li>Equipamentos esportivos: tacos de golfe, pranchas de surfe, esquis e raquetes de tênis são objetos que possuem mais dificuldade de serem armazenados, mas um self box pode ser a melhor alternativa para isso;</li><li>Coleções: pessoas que possuem coleções muito extensas ou de objetos muito grandes podem precisar de um espaço extra para conseguir armazenar esses itens acumulados;</li><li>Estoque: lojas que fazem venda online possuem um depósito em certo lugar para que possam fazer o envio imediato de uma mercadoria para o cliente com facilidade;</li><li>Reformas ou mudanças: quando alguém vai se mudar, pode ser muito mais fácil e cômodo guardar um móvel em um self box para conseguir transportar as coisas aos poucos.</li></ul><h2>Vantagens do self box</h2><p>A segurança desse tipo de serviço é garantida pela presença de câmeras que filmam 24 horas por dia e somente o cliente que contratou o self box tem acesso a essas imagens e às chaves do local. O cliente também pode escolher por quanto tempo ele irá utilizar o espaço extra e o tamanho do local, além de poder acessar seus pertences a hora que bem entender, sem horário determinado.</p>

                    </article>
                <?php include('inc/coluna-lateral.php'); ?>
                <br class="clear" />
                <?php include('inc/paginas-relacionadas.php'); ?>
                <?php include('inc/regioes.php'); ?>
                <br class="clear">
                <?php include('inc/copyright.php'); ?>
            </div>
        </div>    
    <?php include('inc/footer.php'); ?>
</body>
</html>