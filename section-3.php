<section class="portfolio-section my-5 p-0 px-1 portfolio-box"> 
		
    <div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="mb-5 mt-3 col-12 text-center text-destaq " style="color:#000">Galeria de Produtos</h2>
				<span class="line-yellow my-2"></span>
			</div>
		</div>
		
	</div>
	<?php
            $palavraEstudo_s8 = array(            
            'custo de armazenagem',
            'guarda volumes sp',
            'storage guarda tudo',
            'armazenamento de alimentos',
            'guarda móveis rj',
            'guarda tudo',
            'self storage preço',
            'armazenagem e estocagem'
            );
                
            include 'inc/vetKey.php';            
            asort($vetKey); ?>
    
            <div class='portfolio-box container gallery'><div class='row'>
    
            <?php foreach ($vetKey as $key => $value) {
            if(in_array(strtolower($value['key']), $palavraEstudo_s8)){  ?>
                
             
                
					<div class='col-md-3' style='padding:15px;'>
                        <div class='project-post'>						
							<img src='<?= $url; ?>assets/img/img-mpi/350x350/<?=$value['url'];?>-1.jpg' alt='<?=$value['key'];?>' title='<?=$value['key'];?>' style='width:100%;'> 
									<div class='project-content text-center d-flex align-items-center justify-content-center' style='height:60px'>
						              <h2 class='m-0' style='font-size:13px;line-height: 20px;'><?=$value['key'];?></h2>                                        
                                    </div>
							     <div class='hover-box' style='border-radius:0;padding:10px'>
                                     <div class="col-12 p-0 h-100 borderhover">
				                    <a href='<?= $url; ?>assets/img/img-mpi/<?=$value['url'];?>-1.jpg' data-caption='<?=$value['key'];?>' data-fancybox='group2' class='lightbox zoom' >
                                        <i class='fa fa-eye'></i>
                                     </a>
                                     </div>
				                </div>
				        </div>
                    </div>
					
			<?php	} } ?>
        </div>
    </div>
  
</section>
