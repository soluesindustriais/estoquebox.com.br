<?php
$h1 = "Informações";
$title = "informacoes";
$desc = "Faça cotação dos vidros de forma prática. Tenha acesso a uma ampla rede de fornecedores e garanta segurança para o seu produto por um preço que cabe no seu bolso"; 
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php include('inc/head.php'); ?>
</head>

<body>
    <?php include 'inc/breadcrumb-categoria.php' ?>

    <section class="container mb-5 informacoes portfolio-section portfolio-box">
        <div class="row m-0">
            <div class="col-md-12">

                
                <div class='portfolio-box container'>
                    <div class='row' id='example'>
                <?php              include_once('inc/vetKey.php');
                foreach ($vetKey as $key => $vetor) { ?>
                <div class='col-md-3' style='padding:15px;'>
                    <div class='project-post'>
                        <img src='<?=$url?>assets/img/img-mpi/350x350/<?=$vetor['url']?>-1.jpg' alt='<?=$vetor['key']?>' title='<?=$vetor['key']?>' style='width:100%;'>

                        <div class='project-content text-center d-flex align-items-center justify-content-center' style='height:60px'>
                            <h2 class='m-0' style='font-size:13px;line-height: 20px;'><?=$vetor['key']?></h2>
                        </div>
                        <div class='hover-box' style='border-radius:0;padding:10px'>
                            <div class='col-12 p-0 h-100 borderhover'>
                                <a href='<?=$url.$vetor['url']?>' class='zoom'><i class='fa fa-eye'></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <?php } ?>
               </div>
        </div>
            </div>
        </div>


    </section>

    <?php include 'inc/footer.php' ?>
</body>

</html>
