<?php
$h1 = "Quem Somos";
$title = $h1;
$desc = "O Portal Soluções Industriais tem o objetivo de facilitar a busca de produtos e serviços em um único portal. Com ele você tem acesso a diversas ferramentas e todo o controle para uma pesquisa rápida e eficiente...";
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php include('inc/head.php'); ?>
</head>

<body>
    <?php include 'inc/header.php' ?>
	<section class="second-bloc py-5" style="margin-top:-3rem">
		<div class="container">

            

                
				<div class="row">
					
					<div class="col-md-12 mb-4">
                     <div class="quem-somos-bloco-3 w-100 p-0 p-4 h-100">
                        <h2 class="text-sec-block m-0 mb-4 text-center">Qualidade e comprometimento</h2>
                        <p class="text-justify">
                            O Soluções Industriais é a empresa responsável pelo marketplace que leva o mesmo nome, o seu objetivo é facilitar a busca de fornecedores e compradores no meio industrial. Por isso, com essa tecnologia o usuário pode fazer solicitações de orçamentos, comparar produtos e entrar em contato com os fornecedores, apenas acessando o portal. As interações ocorrem de forma rápida e dinâmica com o consumidor final, o que possibilita o aceleramento da sua venda pela internet, além da plataforma estar presente em muitos outros canais digitais e inovar o mercado industrial.

					   </p>
                        </div>
                    </div>
					
            </div>
							
				<div class="row">
                    
					<div class="col-md-4 mb-3">
                        <div class="quem-somos-bloco-3 w-100 p-0 p-4 h-100">
                        <h2 class="text-center">MISSÃO</h2>
						<p class="font-quem-somos text-center">
							O Soluções Industriais tem a missão de tornar a procura por um produto ou serviço mais fácil e dinâmica. A plataforma tem o objetivo de ser assertiva para os fornecedores e aos compradores em, e para tal resultado o Soluções Industriais alinha as suas estratégias às inovações do marketing digital. 

						</p>
                        </div>
					</div>
                    
					<div class="col-md-4 mb-3">
                        <div class="quem-somos-bloco-3 w-100 p-0 p-4 h-100">
                        <h2 class="text-center">VISÃO</h2>
						<p class="font-quem-somos text-center">
                            O Soluções Industriais acredita que a procura por produtos de qualidade possa ser mais fácil e dinâmica, pois quando o consumidor sabe o que procura, está determinado a adquirir o item ou serviço e deseja algo que tenha um valor real e acessível ao que se espera da aquisição.</p>
                        </div>
                    </div>
                    
						<div class="col-md-4 mb-3" > 
                            <div class="quem-somos-bloco-3 w-100 p-0 p-4 h-100">
                            <h2 class="text-center text-center">VALORES</h2>							
                                <ul><li>Compromisso;</li><li>Qualidade;</li><li>Confiança;</li><li>Inovação;</li><li>Eficiência.</li></ul>
                            
                            </div>
						</div>
					</div>
					
						
				<p class="w-100 text-center">
				A tecnologia Ideal Stogare® é uma patente da empresa Soluções Industriais®, integrante do Grupo Ideal Trends®
				</p>
						
				
        </div>
				</section>
			

    <?php include 'inc/footer.php' ?>
</body>

</html>
