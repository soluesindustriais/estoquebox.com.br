<section>
    <div class="container-fluid">
        <div class="row">
          
            <div class="col-12 background-bloco py-5 p-md-5 align-items-center justify-content-center">
                <div class="col-12 col-md-8">               
                    <h2 class="text-destaq text-center">Informações</h2>
                    <p class="sec-p text-dark text-center">O Self Storage se tornou uma opção para guardar peças de valor e em grandes quantidades para empresas e pessoas físicas. O objetivo desse serviço é armazenar itens de forma segura e privativa. Além do Self Storage, existe o Guarda Volumes, que atende pessoas que desejam guardar documentos ou peças pequenas.</p>

                    <p class="sec-p text-dark text-center">Além da possibilidade de poder armazenar itens pequenos, existe o Guarda Móveis que como você deve imaginar, permite que itens maiores sejam guardados.</p>

                    <p class="sec-p text-dark text-center">O Self Storage permitiu que famílias realizassem mudanças e reformas mais tranquilas e as corporações que armazenassem seus documentos confidenciais de forma segura.</p>



                    <div class="row d-flex justify-content-center">

                        <div class="col-md-4 col-sm-12" style="padding:15px">
                            <div class="quadro-depo m-0 d-flex w-100 align-items-center justify-content-center">

                                <img src="<?=$url?>assets/img/icons/bank.png" width="100" alt="Segurança">
                                <p class="mt-3 mb-0">Segurança</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12" style="padding:15px">
                            <div class="quadro-depo m-0 d-flex w-100 align-items-center justify-content-center">

                                <img src="<?=$url?>assets/img/icons/box.png" width="100" alt="Armazenamento">
                                <p class="mt-3 mb-0">Armazenamento</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12" style="padding:15px">
                            <div class="quadro-depo m-0 d-flex w-100 align-items-center justify-content-center">

                                <img src="<?=$url?>assets/img/icons/cart.png" width="100" alt="Inovação">
                                <p class="mt-3 mb-0">Inovação</p>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center mt-2 mb-4">
                            <a href="<?=$url?>informacoes" class="button-slider2">Saiba Mais</a>
                        </div>
                    </div>
                </div>
            </div>




        </div>

    </div>
</section>
