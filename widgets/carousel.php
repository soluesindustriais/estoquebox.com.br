<div id="carousel" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators lineSlide">
		<li data-target="#carousel" data-slide-to="0" class="active"></li>
		<li data-target="#carousel" data-slide-to="1"></li>
		<li data-target="#carousel" data-slide-to="2"></li>
	</ol>

	<div class="carousel-inner">
		<div class="carousel-item active one">
			<div class="d-flex scont">
                <div class="container d-flex justify-content-md-end justify-content-center align-items-center">
                    <div class="col-md-7 col-11 p-3 mr-md-5">
				<h2 class="stitulo">self storage</h2>
				<p class="sdesc">É o armazém privativo para empresas e pessoas</p>
				<a href="<?php if (file_exists(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."self-storage.php")) { echo $url."self-storage"; } else { echo $url."informacoes"; } ?>" class="sbutton col-md-6 col-12">Saiba Mais <i class="fas fa-angle-double-right"></i></a>
                    </div>
                </div>
			</div>
		</div>	
		<div class="carousel-item three">
			<div class="d-flex scont">
                <div class="container d-flex justify-content-md-end justify-content-center align-items-center">
                    <div class="col-md-7 col-11 p-3 mr-md-5">
				<h2 class="stitulo" >guarda volumes</h2>
				<p class="sdesc">É uma opção para quem deseja armazenar coisas de pequeno porte</p>
				<a href="<?php if (file_exists(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."guarda-volumes.php")) { echo $url."guarda-volumes"; } else { echo $url."informacoes"; } ?>" class="sbutton col-md-6 col-12">Saiba Mais <i class="fas fa-angle-double-right"></i></a>
                    </div>
			</div>
            </div>
		</div>
        <div class="carousel-item two">
			<div class="d-flex scont">
                <div class="container d-flex justify-content-md-end justify-content-center align-items-center">
                    <div class="col-md-7 col-11 p-3 mr-md-5">
				<h2 class="stitulo" >guarda móveis</h2>
				<p class="sdesc" >É ideal para armazenar peças de grande porte ou em grandes quantidades</p>
				<a href="<?php if (file_exists(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."guarda-moveis.php")) { echo $url."guarda-moveis"; } else { echo $url."informacoes"; } ?>" class="sbutton col-md-6 col-12">Saiba Mais <i class="fas fa-angle-double-right"></i></a>
                    </div>
			</div>
            </div>
		</div>
	</div>

	<a class="carousel-control-prev " href="#carousel" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon seta" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>

	<a class="carousel-control-next " href="#carousel" role="button" data-slide="next">
		<span class="carousel-control-next-icon seta" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>

</div>



